import PyPDF2
import os

emptyPage = open('./empty/empty.pdf', 'rb')
pdfEmptyReader = PyPDF2.PdfFileReader(emptyPage)

pdfFiles = []

for filename in os.listdir('.'):
    if filename.endswith('.pdf'):
        pdfFiles.append(filename)
pdfFiles.sort(key=str.lower)

for filename in pdfFiles:
    pdfFileObj = open(filename, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    pdfNumPages = pdfReader.numPages
    if (pdfNumPages % 2) != 0:
        pdfWriter = PyPDF2.PdfFileWriter()
        for pageNum in range(pdfReader.numPages):
            pageObj = pdfReader.getPage(pageNum)
            pdfWriter.addPage(pageObj)
        
        pageObj = pdfEmptyReader.getPage(0)
        pdfWriter.addPage(pageObj)

        pdfOutputFile = open('./even/' + filename, 'wb')
        pdfWriter.write(pdfOutputFile)
        pdfOutputFile.close()
    pdfFileObj.close()
